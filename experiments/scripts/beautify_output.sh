#!/bin/bash

# Temporary script to group traces o the results to be easily exploitable
# by visu sripts.

# for filename in  $(ls temp/results)
# do
#   wl=$(echo $filename| cut -d'_' -f 1-3 )
#   rej=$(echo $filename| cut -d'_' -f 4)
#   # mkdir temp/exp_by_workloads/$wl/$rej
#   cp -r temp/results/$filename/* temp/exp_by_workloads/$wl/$rej/
# done

#for filename in  $(ls temp/results)
#do
#  new_name=$(echo $filename| cut -d'_' -f 1-3 )
#  mkdir -p temp/exp_by_workloads/$new_name
#done

# for folder in $(ls temp/fcfs)
# do
#   wl=${folder%.*}
#   mkdir temp/exp_by_workloads/$wl/fcfs
#   cp temp/fcfs/$wl.json/*  temp/exp_by_workloads/$wl/fcfs
# done

for folder in $(ls temp/exp_by_workloads)
do
  for th in t5 t50 t10 t100 t500
  do
    echo $folder/$th
    trace=temp/exp_by_workloads/$folder/$th
    jobs='$jobs "python3 scripts/merge_killed_jobs.py -f $trace/out_jobs.csv -o $trace/merged_jobs.csv"'
  done
done
pwd

parallel ::: parallel -k echo ::: A B C > abc-file
