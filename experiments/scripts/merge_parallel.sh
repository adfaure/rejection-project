#!/bin/bash

results=/home/adfaure/Projects/rejection-project/experiments/data/results/group_size_results/save/

simulations=$(find $results -name out_jobs.csv| xargs /bin/dirname)

parallel --verbose -k python3 merge_killed_jobs.py -f "{1}/out_jobs.csv" -o {1}/merged_jobs.csv ::: $simulations
