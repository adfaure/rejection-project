{
  pkgs ? import <nixpkgs> {},
  dmnix ? import ./datamove-nix {},
}:
let
  callPackage = pkgs.lib.callPackageWith (pkgs // pkgs.xlibs // self);
  self = rec {

    meta-sched_real = callPackage ./schedulers/meta-sched/meta-sched.nix {};

    meta-sched = pkgs.lib.overrideDerivation meta-sched_real (oldAttrs:
      {src=./schedulers/meta-sched;});

    batsim = pkgs.lib.overrideDerivation dmnix.batsim (oldAttrs : {
      src = ./batsim;
    });

   jupyter_env=pkgs.stdenv.mkDerivation {
        name = "expe";
        propagatedBuildInputs =
        [
          pkgs.python36
          pkgs.python36Packages.jupyter
          pkgs.python36Packages.pyqt5
          pkgs.python36Packages.pandas
          pkgs.python36Packages.docopt
          pkgs.python36Packages.matplotlib
          pkgs.python36Packages.pip
          dmnix.evalys
        ];
      };

    dev=pkgs.stdenv.mkDerivation {
        name = "dev";
        propagatedBuildInputs =
        [
          dmnix.batsim.buildInputs
          dmnix.batsim.nativeBuildInputs
          dmnix.coloredlogs
          dmnix.humanfriendly
          pkgs.zeromq
          pkgs.python36
          pkgs.redis
          pkgs.rustc
          pkgs.cargo
          pkgs.pkgconfig
          pkgs.python36Packages.async-timeout
          pkgs.python36Packages.asyncio
          pkgs.python36Packages.colorlog
          pkgs.python36Packages.pandas
          pkgs.python36Packages.pyyaml
          pkgs.python36Packages.colored
        ];
      };

    exp_env=pkgs.stdenv.mkDerivation {
        name = "expe";
        propagatedBuildInputs =
        [
          dev.propagatedBuildInputs
          batsim
          meta-sched
        ];
      };
  };

in
  self

